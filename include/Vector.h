//
// Created by tapa.
//

#ifndef VECTOR_H
#define VECTOR_H

#include <math.h>
#include <vector>

class Vector {
//attribut : tableau de taille trois qui correspond au coordonnées d'un point 3D (x, y, z)
private:
    double pnt3D[3];

public:
    //constructeur
    Vector(double x=0, double y=0, double z=0) {
        pnt3D[0] =x;
        pnt3D[1] =y;
        pnt3D[2] =z;
    }
    //creation d'un accesseur constant et non constant
    const double& operator[](int i) const { return pnt3D[i];}
    double& operator[](int i) { return pnt3D[i];}
    //fonction qui renvoi la norme au carré d'un vecteur
    double normCarre() {
        return pow(pnt3D[0], 2) + pow(pnt3D[1], 2) + pow(pnt3D[2], 2);
    }
    //fonction qui calcule la norme dun vecteur
    void norme() {
        double norm = sqrt(normCarre());
        pnt3D[0] /= norm;
        pnt3D[1] /= norm;
        pnt3D[2] /= norm;
    }
    //fonction qui calcule la norme au carre du vecteur courant
    Vector normVecCourant() {
        Vector result(*this);
        result.norme();
        return result;
    }
    Vector& operator+=(const Vector& b) {
        pnt3D[0] += b[0];
        pnt3D[1] += b[1];
        pnt3D[2] += b[2];
        return *this;
    }
};
//definitions des methodes
Vector operator+(const Vector& a, const Vector& b);
Vector operator-(const Vector& a, const Vector& b);
Vector operator*(double a, const Vector& b) ;
Vector operator*(const Vector& a, const Vector& b);
Vector operator*(const Vector& a,double b) ;
Vector operator/(const Vector& a, double b);
double dot( const Vector& a, const Vector& b);
Vector cross( const Vector& a, const Vector& b);
Vector random_cos(const Vector& N);
/**
 * Definition de la classe rayon
 * @description :  il permet de generer des rayons. Et rayon est constitué d'un origine et d'une direction
 */
class Ray {
public:
    //attributs
    Vector origin;
    Vector direction;

    //constructeur
    Ray(const Vector& o, const Vector& d) : origin(o), direction(d) {};

};
#endif //VECTOR_H
