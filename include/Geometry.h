//
// Created by tapa.
//
#ifndef GEOMETRY_H
#define GEOMETRY_H

#pragma once

#include <iostream>
#include <vector>
#include "Vector.h"
#include <algorithm>

class BBox {
public :
    //attributs
    Vector bmin, bmax;
    BBox() {};  //constructeur par défaut
    BBox(const Vector& bmin, const Vector& bmax): bmin(bmin), bmax(bmax) {}; //constructeur
    //routine d'intersection
    bool intersection(const Ray& d) const {
        double minX = std::min((bmin[0] - d.origin[0]) / d.direction[0], (bmax[0] - d.origin[0]) / d.direction[0]);
        double maxX = std::max((bmin[0] - d.origin[0]) / d.direction[0], (bmax[0] - d.origin[0]) / d.direction[0]);
        double minY = std::min((bmin[1] - d.origin[1]) / d.direction[1], (bmax[1] - d.origin[1]) / d.direction[1]);
        double maxY = std::max((bmin[1] - d.origin[1]) / d.direction[1], (bmax[1] - d.origin[1]) / d.direction[1]);
        double minZ = std::min((bmin[2] - d.origin[2]) / d.direction[2], (bmax[2] - d.origin[2]) / d.direction[2]);
        double maxZ = std::max((bmin[2] - d.origin[2]) / d.direction[2], (bmax[2] - d.origin[2]) / d.direction[2]);
        if(std::min(std::min(maxX, maxY), maxZ) - std::max(std::max(minX, minY), minZ ) >0) return true;
        return false;
    }
};

//class object
class Object {
public:
    Object() {};
    virtual bool intersection(const Ray& d, Vector& P, Vector& N, double& t) const = 0;
    Vector albedo;
    bool miroir;
    bool transparent;

};

class BVH {
public:
    int i0, i1;
    BBox bbox;
    BVH *fg, *fd;

};
/**
 * Classe Geomtrie qui herite de Object
*/
class Geometry : public Object{
private :
    BVH bvh;
public:
    Geometry(const char* obj, double scaling, const Vector& offset, const Vector &couleur, bool mirror = false, bool transp = false);
    std::vector<int> faceGroup;
    std::vector<int> faces;
    std::vector<int> normalIds;
    std::vector<int> uvIds;
    std::vector<Vector> vertices;
    std::vector<Vector> normals;
    std::vector<Vector> uvs; //vector en 3d mais on n'utilise que deuux composantes
    bool intersection(const Ray& d, Vector& P, Vector& N, double& t) const;
    BBox build_bbox(int i0, int i1);
    void build_bvh(BVH* node, int i0, int i1);
};
/**
 * Classe sphere
 * Un sphere est constitué d'une origine O et d'unrayon R et une routine d'intersection des spheres courantes
*/
class Sphere : public Object {
public :
    //attributs
    Vector O; //origine
    double R; //rayon
    Sphere(const Vector& origin, double rayon, const Vector& couleur, bool mirror = false, bool transp = false) : O(origin), R(rayon) {
         albedo = couleur;
         miroir = mirror;
         transparent = transp;
    };
    //routine d'intersection
    bool intersection(const Ray& d, Vector& P, Vector& N, double& t) const{
        //resolution de l'equation du second degres a*t^2 +b*t +c
        double a=1;
        double b= 2*dot(d.direction, d.origin - O);
        double c = (d.origin - O).normCarre() - R*R;
        double delta = b*b - 4*a*c;
        double t1 = (-b - sqrt(delta))/(2 * a);
        double t2 = (-b + sqrt(delta))/(2 * a);
        if(delta < 0) return false;
        if(t2 < 0) return false;
        if(t1 > 0) t = t1;
        else t = t2;
        P = d.origin + t*d.direction;
        N = (P - O).normVecCourant();
        return true;
    }
};
/**
 * Classe triangle elle herite de la classe Object
 * un triangle est composé de trois sommets A, B et C
*/
class Triangle : public Object{
public :
    //attributs
    const Vector &A, &B, &C;
    //constructeur
    Triangle(const Vector& A, const Vector& B, const Vector& C, const Vector& couleur, bool mirror = false, bool transp = false) : A(A), B(B), C(C) {
        albedo = couleur;
        miroir = mirror;
        transparent = transp;
    };
    bool intersection(const Ray& d, Vector& P, Vector& N, double& t) const{  //routine d'intersection des spheres courantes
        N  = -1*cross(B - A, C - A).normVecCourant();
        t = dot(C- d.origin, N) / dot(d.direction, N);
        if(t<0) return false;
        P = d.origin + t*d.direction;
        Vector u = B - A;
        Vector v = C - A;
        Vector w = P - A;
        double detm = (u.normCarre() * v.normCarre())  - pow(dot(u, v), 2);
        double beta = ( (dot(w, u) * v.normCarre()) - (dot(w, v) * dot(u, v)) ) / detm;  //coordonn�es barycentrique w,r,t par rapport a B
        double gamma = ((u.normCarre() * dot(w, v)) - (dot(u, v) * dot(w, u))) / detm; //coordonn�es barycentrique par rapport a C
        //si alpha, beta, gamma < 0 alpha, beta, gamma <1 alors P n'est pas à l'interieur
        double alpha = 1 - beta - gamma;
        if(alpha < 0 || alpha >1) return false;
        if(beta < 0 || beta >1) return false;
        if(gamma < 0 || gamma >1) return false;
        return true;
    }
};

//creation de la classe scene
class Scene {
public :
    //attributs
    std:: vector<const Object*> objects;
    Sphere *lumiere;
    double intensite_lumiere;
    //constructeur par defaut
    Scene() {}
    // methode qui permet d'ajouter une sphere dans la scene
    void addSphere( const Sphere& s){objects.push_back(&s);}
    // methode qui permet d'ajouter un triangle dans la scene
    void addTriangle( const Triangle& t){ objects.push_back(&t);}
    // methode qui permet d'ajouter un geometry dans la scene
    void addGeometry( const Geometry& g){ objects.push_back(&g);}
    //routine d'intersection
    bool intersection(const Ray& d, Vector& P, Vector& N, int& sphere_id, double& min_t) const {
        bool intersect = false;
        min_t = 1E99;
        for(int i=0; i<objects.size(); i++ ){ // de maniere local
            Vector localP, localN;
            double t;
            //bool interctLocal =  objects[i]->intersection(d, localP, localN, t);
            if(objects[i]->intersection(d, localP, localN, t)) {
                intersect = true;
                if(t < min_t) {
                    min_t = t;
                    P = localP;
                    N = localN;
                    sphere_id = i;
                }
            }
        }
        return intersect;
    }

};



#endif // GEOMETRY_H
