#include "Geometry.h"
#include <list>

Geometry::Geometry(const char* obj, double scaling, const Vector& offset, const Vector& couleur, bool mirror, bool transp) {

    albedo = couleur;
    miroir = mirror;
    transparent = transp;

    FILE* f;
    f = fopen(obj, "r");
    int curGroup = -1;
    while(!feof(f)) {
        char line[255];
        fgets(line, 255, f);
        if(line[0] == 'u' && line[1] == 's') {
            curGroup++;
        }
        if(line[0] == 'v' && line[1] == ' ') {
            Vector vec;
            //sscanf_s(line, "v %lf %lf %lf\n", &vec[2], &vec[0], &vec[1]); //car
            sscanf(line, "v %lf %lf %lf\n", &vec[0], &vec[2], &vec[1]); //girl
            //sscanf_s(line, "v %lf %lf %lf\n", &vec[0], &vec[1], &vec[2]); //dragon

            vertices.push_back(scaling*vec + offset);
        }
        if(line[0] == 'v' && line[1] == 'n') {
            Vector vec;
            //sscanf_s(line, "vn %lf %lf %lf\n", &vec[2], &vec[0], &vec[1]); //car
            sscanf(line, "vn %lf %lf %lf\n", &vec[0], &vec[2], &vec[1]); //girl
            normals.push_back(vec);
        }
        if(line[0] == 'v' && line[1] == 't') {
            Vector vec;
            sscanf(line, "vt %lf %lf\n", &vec[0], &vec[1]);
            uvs.push_back(vec);
        }
        if(line[0] == 'f') {
           int i0, i1, i2;
           int j0, j1, j2;
           int k0, k1, k2;
           faceGroup.push_back(curGroup);
           int nn = sscanf(line, "f %u/%u/%u/%u/%u/%u/%u/%u/%u\n", &i0, &j0, &k0, &i1, &j1, &k1, &i2, &j2, &k2);
           if(nn == 9) {
            faces.push_back(i0 - 1);
            faces.push_back(i1 - 1);
            faces.push_back(i2 - 1);
            uvIds.push_back(j0 - 1);
            uvIds.push_back(j1 - 1);
            uvIds.push_back(j2 - 1);
            normalIds.push_back(k0 - 1);
            normalIds.push_back(k1 - 1);
            normalIds.push_back(k2 - 1);
           }
           else {
            int i3, j3;
            nn = sscanf(line, "f %u/%u/%u/%u/%u/%u/%u/%u\n", &i0, &j0, &i1, &j1, &i2, &j2, &i3, &j3); //le dragon contient des quads
            if(nn == 8) {
                faces.push_back(i0 - 1);
                faces.push_back(i1 - 1);
                faces.push_back(i2 - 1);
                faces.push_back(i0 - 1);
                faces.push_back(i2 - 1);
                faces.push_back(i3 - 1);
                faceGroup.push_back(curGroup);
                uvIds.push_back(j0 - 1);
                uvIds.push_back(j1 - 1);
                uvIds.push_back(j2 - 1);
                uvIds.push_back(j0 - 1);
                uvIds.push_back(j2 - 1);
                uvIds.push_back(j3 - 1);
            }
            else {
                nn = sscanf(line, "f %u/%u/%u/%u/%u/%u\n", &i0, &j0, &i1, &j1, &i2, &j2);
                faces.push_back(i0 - 1);
                faces.push_back(i1 - 1);
                faces.push_back(i2 - 1);
                uvIds.push_back(j0 - 1);
                uvIds.push_back(j1 - 1);
                uvIds.push_back(j2 - 1);
            }
           }
        }

    }
    fclose(f);
    build_bvh(&bvh, 0, faces.size() / 3);
}

BBox Geometry::build_bbox(int i0, int i1) {
    BBox result;
    result.bmax = vertices[faces[i0 * 3]];
    result.bmin = vertices[faces[i0 * 3]];
    for(int i= i0; i< i1; i++) { //indice de triangle
        for(int j=0; j<3; j++) { //indice du sommet
            for(int k =0; k<3; k++) {  //indice de dimension
                result.bmin[k] = std::min(result.bmin[k], vertices[faces[i * 3 + j]][k]);
                result.bmax[k] = std::max(result.bmax[k], vertices[faces[i * 3 + j]][k]);
            }
        }
    }
    return result;
}
void Geometry::build_bvh(BVH* node, int i0, int i1) {
    node->bbox = build_bbox(i0, i1);
    node->i0 = i0;
    node->i1 = i1;
    node->fg = NULL;
    node->fd = NULL;
    Vector diag = node->bbox.bmax - node->bbox.bmin;
    int split_dim;
    if( (diag[0] > diag[1]) && (diag[0] > diag[2])) split_dim = 0;
    else {
        if( (diag[1] > diag[0]) && (diag[1] > diag[2])) split_dim = 1;
        else split_dim = 2;
    }
    double split_val = node->bbox.bmin[split_dim] + diag[split_dim]*0.5;
    int pivot = i0 - 1;
    for(int i=i0; i < i1; i++) {
        double center_split_dim = (vertices[faces[i*3]][split_dim] + vertices[faces[i*3 + 1]][split_dim] + vertices[faces[i*3 + 2]][split_dim]) / 3.;
        if(center_split_dim < split_val) {
            pivot++;
            std::swap(faces[i * 3 +0], faces[pivot * 3 + 0]);
            std::swap(faces[i * 3 +1], faces[pivot * 3 + 1]);
            std::swap(faces[i * 3 +2], faces[pivot * 3 + 2]);
        }
    }
    if(pivot <= i0 || pivot >= i1) return;
    node->fg = new BVH();
    node->fd = new BVH();
    build_bvh(node->fg, i0, pivot);
    build_bvh(node->fd, pivot, i1);
}

bool Geometry::intersection(const Ray& d, Vector& P, Vector& N, double &t) const {
    bool intersect = false;
    t = 1E99;
    if(!bvh.bbox.intersection(d)) return false;
    std::list<const BVH*> liste;
    liste.push_front(&bvh);
    while (!liste.empty()) {
        const BVH* courant = liste.front();
        liste.pop_front();
        if(courant->fg && courant->fg->bbox.intersection(d)) liste.push_back(courant->fg);
        if(courant->fd && courant->fd->bbox.intersection(d)) liste.push_back(courant->fd);
        if(!courant->fg) {
            for(int i= courant->i0; i< courant->i1; i++) {
                Vector Plocal, Nlocal;
                double tlocal;
                Triangle triangle(vertices[faces[i * 3]], vertices[faces[i * 3 + 1]], vertices[faces[i * 3 + 2]], albedo, miroir, transparent);
                if(triangle.intersection(d, Plocal, Nlocal, tlocal)) {
                    if(tlocal < t) {
                        t = tlocal;
                        P = Plocal;
                        N = Nlocal;
                    }
                    intersect = true;
                }
            }

        }
    }
    return intersect;
}

