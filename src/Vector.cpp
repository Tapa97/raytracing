#include "../include/Vector.h"
#include <random>

#define M_PI 3.1415926535897932

static std::default_random_engine engine;
static std::uniform_real_distribution<double> unifom(0,1);

//methode permettant l'addition de deux vecteurs a et b
Vector operator+(const Vector & a, const Vector & b) {
    return Vector(a[0]+b[0], a[1]+b[1], a[2]+b[2]);
}
//soustraction de deux vecteurs a et b
Vector operator-(const Vector& a, const Vector& b) {
    return Vector(a[0]-b[0], a[1]-b[1], a[2]-b[2]);
}
/** surcharge de l'operation multiplication */
//multiplication entre un flottant et un vecteur
Vector operator*(double a, const Vector& b) {
    return Vector(a*b[0], a*b[1], a*b[2]);
}
//produit  de deux vecteurs
Vector operator*(const Vector& a, const Vector& b) {
    return Vector(a[0]*b[0], a[1]*b[1], a[2]*b[2]);
}
//produit entre un vecteur et un flottant
Vector operator*(const Vector& a,double b) {
    return Vector(a[0]*b, a[1]*b, a[2]*b);
}
//division entre un vecteur et un flottant
Vector operator/(const Vector& a, double b) {
    return Vector(a[0]/b, a[1]/b, a[2]/b);
}
//produit scalaire entre deux vecteurs
double dot( const Vector& a, const Vector& b) {
    return a[0]*b[0] + a[1]*b[1] + a[2]*b[2];
}
//produit vectoriel qui renvoi un vecteur
Vector cross( const Vector& a, const Vector& b) {
    return Vector(a[1]*b[2] - a[2]*b[1], a[2]*b[0] - a[0]*b[2], a[0]*b[1] - a[1]*b[0] );
}
//generation d'un rayon aléatoire  en fonction de la direction (wi, N) sois cosinus
Vector random_cos(const Vector& N) {
    double r1 = unifom(engine);
    double r2 = unifom(engine);
    double x = cos(2*M_PI*r1) * sqrt(1 - r2);
    double y = sin(2*M_PI*r1)*sqrt(1 - r2);
    double z = sqrt(r2);
    Vector directAlea(x, y, z);  //direction aléatoire en repere local
    Vector vecAlea(unifom(engine) - 0.5, unifom(engine) - 0.5, unifom(engine) - 0.5);  //vecteur aléatoire uniforme
    Vector tan = cross(N, vecAlea);   //generation d'une tangeante entre la normale et le vecteur aleatoire
    tan.norme();
    return (directAlea[2] * N + directAlea[0] * tan + directAlea[1] * cross(tan, N));
}


