#include <iostream>
#include <vector>
#include <cmath>
#include "Vector.h"
#include "Geometry.h"
#include <random>
#include <omp.h>


#define M_PI 3.1415926535897932

std::default_random_engine engine;
std::uniform_real_distribution<double> unifom(0,1);

void save_image(const char* filename, const unsigned char* pixels, int W, int H) { // stored as RGB

    unsigned char bmpfileheader[14] = { 'B','M', 0,0,0,0, 0,0, 0,0, 54,0,0,0 };
    unsigned char bmpinfoheader[40] = { 40,0,0,0, 0,0,0,0, 0,0,0,0, 1,0, 24,0 };
    unsigned char bmppad[3] = { 0,0,0 };

    int filesize = 54 + 3* W*H;
    bmpfileheader[2] = (unsigned char)(filesize);
    bmpfileheader[3] = (unsigned char)(filesize >> 8);
    bmpfileheader[4] = (unsigned char)(filesize >> 16);
    bmpfileheader[5] = (unsigned char)(filesize >> 24);

    bmpinfoheader[4] = (unsigned char)(W);
    bmpinfoheader[5] = (unsigned char)(W >> 8);
    bmpinfoheader[6] = (unsigned char)(W >> 16);
    bmpinfoheader[7] = (unsigned char)(W >> 24);
    bmpinfoheader[8] = (unsigned char)(H);
    bmpinfoheader[9] = (unsigned char)(H >> 8);
    bmpinfoheader[10] = (unsigned char)(H >> 16);
    bmpinfoheader[11] = (unsigned char)(H >> 24);

    FILE* f;
    f = fopen(filename, "wb");
    fwrite(bmpfileheader, 1, 14, f);
    fwrite(bmpinfoheader, 1, 40, f);
    std::vector<unsigned char> bgr_pixels(W*H * 3);
    for (int i = 0; i < W*H; i++) {
        bgr_pixels[i * 3] = pixels[i*3 + 2];
        bgr_pixels[i * 3+1] = pixels[i*3 + 1];
        bgr_pixels[i * 3+2] = pixels[i*3];
    }
    for(int i =0; i<H; i++) {
        fwrite(&bgr_pixels[0] + (W*(H - i -1) * 3), 3, W, f);
        fwrite(bmppad, 1, (4 - (W * 3) % 4) % 4, f);
    }
    fclose(f);
}

Vector getColor(const Ray &r, const Scene &s, int nbrebonds) {
    if(nbrebonds == 0) return Vector(0,0,0);
    Vector P, N;
    int sphere_id;
    double t;
    bool intersect = s.intersection(r, P, N, sphere_id, t);
    Vector intPixel(0,0,0);
    if(intersect) {
        if(s.objects[sphere_id]->miroir) {
            intPixel = getColor(Ray(P+ 0.001*N, r.direction - 2*dot(N, r.direction)*N), s, nbrebonds - 1);
        } else if (s.objects[sphere_id]->transparent) {
            double n1= 1;
            double n2= 1.3;
            Vector Ntransp(N);
            if(dot(r.direction, N)>0) {
                n1 =1.3;
                n2 = 1;
                Ntransp = -1*N;
            }
            double racine = 1 - pow(n1/n2, 2)*(1 - pow(dot(Ntransp, r.direction), 2));
            if(racine>0) intPixel = getColor(Ray(P - 0.001*Ntransp, (n1/n2)*(r.direction - dot(r.direction, Ntransp)*Ntransp) - Ntransp*sqrt(racine)), s, nbrebonds - 1);
        }
        else {
            Vector dirAlea = random_cos((P - s.lumiere->O).normVecCourant());
            Vector wi = ((dirAlea * s.lumiere->R + s.lumiere->O) -P ).normVecCourant();
            double dirCarre = ((dirAlea * s.lumiere->R + s.lumiere->O) - P).normCarre();
            Vector PL, NL;
            int sphere_idL;
            double tL;
            if(s.intersection(Ray(P + 0.01*N, wi), PL, NL, sphere_idL, tL) && tL*tL < dirCarre*0.99) intPixel = Vector(0,0,0);
            else intPixel = (s.intensite_lumiere / (4*M_PI*dirCarre)* std::max(0., dot(N, wi))* dot(dirAlea, -1*wi) / dot((P - s.lumiere->O).normVecCourant(), dirAlea)) * s.objects[sphere_id]->albedo;
            intPixel += getColor(Ray(P + 0.001*N, random_cos(N)), s, nbrebonds - 1) * s.objects[sphere_id]->albedo;   //ajout de la contribution indirecte
        }
    }
    return intPixel;
}

int main() {
    int W = 1024;
    int H = 1024;
    const int nrays = 8;
    double fov = 60 * M_PI / 180;
    //creation d'une source lumineuse
    Sphere slum(Vector(15, 70, -30), 15, Vector(1., 1., 1.));
    //creation des spheres composant la scene
    Sphere s2(Vector(0,-2000-20,0), 2000, Vector(1,1,1)); //sol
    Sphere s3(Vector(0,2000 + 100,0), 2000, Vector(1,1,1)); //plafond
    Sphere s4(Vector(-2000-50, 0,0), 2000, Vector(0,1,0)); //mur gauche
    Sphere s5(Vector(2000 + 50, 0,0), 2000, Vector(1,0,0)); //mur droit
    Sphere s6(Vector(0, 0,-2000-100), 2000, Vector(0,1,1)); //mur au fond cyan
    //creation d'une geometry
    Geometry g("img/BeautifulGirl.obj", 22, Vector(0, -21, -55), Vector(1., 1., 1.));
    //creation de la scene s et ajout des different composantes de la scene
    Scene s;
    s.addSphere(slum);
    s.addGeometry(g);
    s.addSphere(s2);
    s.addSphere(s3);
    s.addSphere(s4);
    s.addSphere(s5);
    s.addSphere(s6);
    s.lumiere = &slum;
    s.intensite_lumiere = 1000000000;
    std::vector<unsigned  char> image(W*H*3);
    #pragma  omp parallel for
    for(int i=0; i<H; i++) {
        for(int j =0; j<W;j++) {
            Vector couleur(0., 0., 0.);
            for(int k=0; k<nrays; k++) {
                //mehode de box muller
                double r1 = unifom(engine);
                double r2 = unifom(engine);
                Vector direction(j - W/2 + 0.5 + (sqrt(-2*log(r1)) * cos(2*M_PI*r2)), i - H/2 + 0.5 + (sqrt(-2*log(r1)) * sin(2*M_PI*r2)), -W/(2*tan(fov/2)));
                direction.norme();
                Ray r(Vector(0., 0., 0.) + Vector((unifom(engine) - 0.5) * 0.5, (unifom(engine) - 0.5) * 0.5, 0), ((20 * direction) - Vector((unifom(engine) - 0.5) * 0.5, (unifom(engine) - 0.5) * 0.5, 0)).normVecCourant());
                couleur += getColor(r, s, 1)/nrays;
            }
            image[((H - i - 1)*W + j ) * 3 +0] = std::min(255., std::max(0., std::pow(couleur[0], 1./2.2))); //rouge
            image[((H - i - 1)*W + j ) * 3 +1] = std::min(255., std::max(0., std::pow(couleur[1], 1./2.2))); //vert
            image[((H - i - 1)*W + j ) * 3 +2] = std::min(255., std::max(0., std::pow(couleur[2], 1./2.2))); //bleu
        }
    }
    save_image("img22bbbb.bmp", &image[0], W, H);
    return 0;
}
